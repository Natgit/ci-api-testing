import requests


# 1
def test_default_beers():
    response = requests.get("https://api.punkapi.com/v2/beers")
    body = response.json()
    assert body[0]["id"] == 1
    assert body[-1]["id"] == 25
    assert len(body) == 25


# 2
def test_id_123():
    response = requests.get("https://api.punkapi.com/v2/beers/123")
    body = response.json()
    assert body[0]["id"] == 123


# 3
def test_get_20_results_from_5_page():
    response = requests.get("https://api.punkapi.com/v2/beers?page=5&per_page=20")
    body = response.json()
    assert len(body) == 20
    assert body[0]["id"] == 81
    assert body[-1]["id"] == 100


# 4
def test_ids_11_to_20():
    params = {
        "ids": "11|12|13|14|15|16|17|18|19|20"
    }
    response = requests.get("https://api.punkapi.com/v2/beers", params=params)
    body = response.json()
    assert len(body) == 10
    beer_id = 11
    for beer in body:
        assert beer["id"] == beer_id
        beer_id += 1


# 5
def test_abv_is_between_5_to_7():
    response = requests.get("https://api.punkapi.com/v2/beers?abv_gt=4.9&abv_lt=7.1")
    body = response.json()
    for beer in body:
        assert beer["abv"] <= 7
        assert beer["abv"] >= 5


# 6
def test_brewed_in_2010():
    response = requests.get("https://api.punkapi.com/v2/beers?brewed_before=01-2011&brewed_after=12-2009")
    body = response.json()
    for beer in body:
        first_brewed = beer["first_brewed"][-4:]
        assert first_brewed == str(2010)
